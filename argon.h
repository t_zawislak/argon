#ifndef _argon_h_
#define _argon_h_

#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <random>
#include <string.h>

#define PI 3.14159

// I/O FILE PATHS
const std::string r_output_path = "output/r_output";
const std::string r_output_path_init = "output/r_output_init.dat";
const std::string p_output_path = "output/p_output";
const std::string F_output_path = "output/F_output";
const std::string State_output_path = "output/State_output";
const std::string log_output_path = ".log/last_run.txt";
const std::string params_input_path = "input/params.txt";

// RANDOM NUMBER GENERATOR:
static std::random_device rd;
static std::mt19937 gen(rd());

class Parameters{
  public:
    int N, n, S_o, S_d, S_out, S_xyz;
    double k, m, e, R, f, L, a, T_0, tau;
    Parameters();
    ~Parameters();
};

class State{
  public:
    double P=0, V=0, H=0, T=0;   // PRESSURE, POTENTIAL, HAMILTONIAN, TEMPERATURE
    double* p[3];
    double* r[3];
    double* F[3];
    int N; // THE SAME N AS IN PARAMS, BUT HELD HERE FOR CONVENIENCE
    State();
    State(int _N);
    ~State();
    void printMomenta(FILE*, double t);
    void printCoordinates(FILE*, double t);
    void printForces(FILE*, double t);
    void printPVHTt(FILE*, double t);
};

// FUNCTIONS
void SetInitialCoordAndMomenta(Parameters pars, State &st);
void OneIteration(Parameters pars, State &st);
void CalculateVPF(Parameters pars, State &st);

double potentialS( double f, double L, double ri );
double potentialP( double R, double rij, double epsilon );
double forceS( double f, double L, double ri, double rii );
double forceP( double R, double rij, double epsilon, double riMrj );

#endif
