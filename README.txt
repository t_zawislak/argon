Author: Tomasz Zawislak

Simulation of Argon crystal

main.cpp	the body
argon.h		all declarations
argon.cpp	class method definitions
functions.cpp	all functions used to conduct calculations


OpenGL visualisation:
./animateAr ../input/params.txt ../output/r_output.dat
