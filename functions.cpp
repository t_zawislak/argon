#include "argon.h"

void OneIteration(Parameters pars, State &st){
  double Ekin=0;

  for (int i = 0; i < pars.N; i++)
    Ekin += pow(st.p[0][i], 2) + pow(st.p[1][i], 2) + pow(st.p[2][i], 2) ;

  for (int i = 0; i < pars.N; i++) {
    // UPDATE MOMENTA AND COORDINATES
    for (int i3 = 0; i3 < 3; i3++){
      st.p[i3][i] += 0.5 * st.F[i3][i] * pars.tau; //         p(t+0.5 tau)
      st.r[i3][i] += 1./pars.m * st.p[i3][i] * pars.tau; //   r(t+tau)
    }
  }


  //----------------- CALALCULATE PRESSURE AND POTENTIAL -----------------------
  CalculateVPF(pars, st);
  //----------------------------------------------------------------------------
  for (int i = 0; i < pars.N; i++)
    for (int i3 = 0; i3 < 3; i3++) st.p[i3][i] += 0.5 * st.F[i3][i] * pars.tau; //         p(t+ tau)

  // ---------------- CALCULATE TEMPERATURE AND TOTAL ENERGY  ------------------
  st.T = 2./(3.*pars.N*pars.k*2.*pars.m) * Ekin;
  //std::cout << "T:" << st.T << "V: " << st.V << std::endl;
  st.H = Ekin / (2.*pars.m) + st.V;

}






void CalculateVPF(Parameters pars, State &st){
  st.V = 0;
  st.P = 0;
  //  SINGLE PARTICLE INTERACTION LOOP
  for (int i = 0; i < pars.N; i++) {
    // |r_i|
    double riabs = sqrt( st.r[0][i]*st.r[0][i] + st.r[1][i]*st.r[1][i] + st.r[2][i]*st.r[2][i] );
    // V_s
    st.V += potentialS( pars.f, pars.L, riabs );
    // F_s
    for (int i3 = 0; i3 < 3; i3++) st.F[i3][i] = forceS( pars.f, pars.L, riabs, st.r[i3][i]) ;
    // PRESSURE
    st.P += 1./(4. * PI * pars.L * pars.L) * sqrt( st.F[0][i]*st.F[0][i] + st.F[1][i]*st.F[1][i] + st.F[2][i]*st.F[2][i] );
    //  INTER-PARTICLE INTERACTION LOOP
    for (int j = 0; j < i; j++) {
      // |r_i - r_j|
      double rij = sqrt( pow(st.r[0][i]-st.r[0][j], 2) + pow(st.r[1][i]-st.r[1][j], 2) + pow(st.r[2][i]-st.r[2][j], 2) );
      // V_p
      st.V += potentialP( pars.R, rij, pars.e );
      // F_p
      for (int i3 = 0; i3 < 3; i3++){
        double dF = forceP( pars.R, rij, pars.e, st.r[i3][i]-st.r[i3][j]);
        //std::cout << dF << std::endl;
        st.F[i3][i] += dF;
        st.F[i3][j] -= dF;

      }
    }


  }//
//  for (int i = 0; i < pars.N; i++) std::cout << i << '\t' <<
//  sqrt( st.F[0][i]*st.F[0][i] + st.F[1][i]*st.F[1][i] + st.F[2][i]*st.F[2][i] ) << std::endl;

}



void SetInitialCoordAndMomenta(Parameters pars, State &st){
  double p_CM[3] = {0., 0., 0.}; // CENTER OF MASS MOMENTUM
  // SET INITIAL VALUES OF MOMENTA AND COORDINATES
  for(int i0=0; i0<pars.n; ++i0){
    for(int i1=0; i1<pars.n; ++i1){
      for(int i2=0; i2<pars.n; ++i2){
        // COORDINATES
        int i = i0 + pars.n*i1 + pars.n*pars.n*i2; // ITERATOR THROUGH ALL PARTIICLES

        st.r[0][i] = pars.a/2. * (2.*i0 + i1 + i2 - 2.*pars.n + 2.);
        st.r[1][i] = pars.a*sqrt(3)/2. * (i1 - pars.n/2. + 0.5) + pars.a*sqrt(3)/6. * (i2 - 0.5*pars.n + 0.5);
        st.r[2][i] = pars.a*sqrt(2./3.) * (i2 - 0.5*pars.n + 0.5);


        // MOMENTA
        std::uniform_real_distribution<double> dis(0.0,1.0);
        for (int i3 = 0; i3 < 3; ++i3) st.p[i3][i] = sqrt( -1* pars.m * pars.k * pars.T_0 * log( dis(gen) ) );
        // SIGN OF PARTICULAR MOMENTUM COORDINATES
        for (int i3 = 0; i3 < 3; ++i3) st.p[i3][i] *= ( dis(gen) < 0.5 ) ? -1 : 1;
        // CALCULATE CENTER OF MASS MOMENTUM
        for (int i3 = 0; i3 < 3; ++i3) p_CM[i3] += st.p[i3][i];
      }
    }
  }

  // SHIFT ALL MOMENTUM COORDINATES TO ENSURE CENTER OF MASS BEING AT REST
  // AND PRINT THE RESULTS
  for(int i=0; i<pars.N; ++i) for (int i3 = 0; i3 < 3; ++i3)  st.p[i3][i] -= p_CM[i3] / pars.N;
}


double potentialS( double f, double L, double ri ){
  if( ri >= L ) return 0.5 * f * (ri-L)*(ri-L);
  return 0.;
}

double potentialP( double R, double rij, double epsilon ){
  return epsilon * ( pow( R / rij , 12.) - 2*pow( R / rij , 6.) ) ;
}

double forceS( double f, double L, double ri, double ri_i ){
  if( ri >= L ) return  f * (L-ri) * ri_i / ri;
  return 0.;
}

double forceP( double R, double rij, double epsilon, double riMrj ){
  //std::cout << 12*epsilon * ( pow( R / rij , 12) - pow( R / rij , 6) ) * riMrj/(rij * rij)  << std::endl;
  return 12*epsilon * ( pow( R / rij , 12.) - pow( R / rij , 6.) ) * riMrj/(rij * rij) ;
}
