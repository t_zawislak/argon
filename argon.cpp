#include "argon.h"

/*

      STATE CLASS

*/

State::State(){}

State::State(int _N){
  N = _N;
  for (int i = 0; i < 3; i++) {
    p[i] = (double*) malloc( sizeof(double)*_N );
    r[i] = (double*) malloc( sizeof(double)*_N );
    F[i] = (double*) malloc( sizeof(double)*_N );
  }
}

State::~State(){
  for (size_t i = 0; i < 3; i++) {
    free( p[i] );
    free( r[i] );
    free( F[i] );
  }
}

void State::printMomenta(FILE* p_output, double t){
  for (int i = 0; i < N; i++)   fprintf(p_output, "%.3le\t%.3le\t%.3le\n", p[0][i], p[1][i], p[2][i]);
}
void State::printCoordinates(FILE* r_output, double t){
  for (int i = 0; i < N; i++)   fprintf(r_output, "%.3le\t%.3le\t%.3le\n", r[0][i], r[1][i], r[2][i]); // add E_kin to saved variables here. E_kin = p^2/2m
  fprintf(r_output, "\n\n");
}
void State::printForces(FILE* F_output, double t){
  for (int i = 0; i < N; i++)   fprintf(F_output, "%.3le\t%.3le\t%.3le\n", F[0][i], F[1][i], F[2][i]);
}
void State::printPVHTt(FILE* St_output, double t){
  //fprintf(St_output, "%.3le\t%.3le\n", t, T );
  fprintf(St_output, "%.3le\t%.3le\t%.3le\t%.3le\t%.3le\n", t, H, V, T, P );
}


/*

      PARAMETERS CLASS

*/

Parameters::~Parameters(){}

Parameters::Parameters(){
  FILE* file_params = fopen(params_input_path.c_str(), "r");
  FILE* file_log    = fopen(  log_output_path.c_str(), "w");
	char s[256];      // comment to be discarded

  fprintf(file_log, "Initial parameters:\n");

	fscanf(file_params, "%i %s", &n, s);
  fprintf(file_log, "%s\t%i\n", s, n);
  fscanf(file_params, "%lf %s", &k, s);
  fprintf(file_log, "%s\t%lf\n", s, k);
  fscanf(file_params, "%lf %s", &m, s);
  fprintf(file_log, "%s\t%lf\n", s, m);
  fscanf(file_params, "%lf %s", &e, s);
  fprintf(file_log, "%s\t%lf\n", s, e);
  fscanf(file_params, "%lf %s", &R, s);
  fprintf(file_log, "%s\t%lf\n", s, R);
  fscanf(file_params, "%lf %s", &f, s);
  fprintf(file_log, "%s\t%lf\n", s, f);
  fscanf(file_params, "%lf %s", &L, s);
  fprintf(file_log, "%s\t%lf\n", s, L);
  fscanf(file_params, "%lf %s", &a, s);
  fprintf(file_log, "%s\t%lf\n", s, a);
  fscanf(file_params, "%lf %s", &T_0, s);
  fprintf(file_log, "%s\t%lf\n", s, T_0);
  fscanf(file_params, "%lf %s", &tau, s);
  fprintf(file_log, "%s\t%lf\n", s, tau);
  fscanf(file_params, "%i %s", &S_o, s);
  fprintf(file_log, "%s\t%i\n", s, S_o);
  fscanf(file_params, "%i %s", &S_d, s);
  fprintf(file_log, "%s\t%i\n", s, S_d);
  fscanf(file_params, "%i %s", &S_out, s);
  fprintf(file_log, "%s\t%i\n", s, S_out);
  fscanf(file_params, "%i %s", &S_xyz, s);
  fprintf(file_log, "%s\t%i\n", s, S_xyz);

  N = n * n * n;

	fclose(file_params);
	fclose(file_log);
}
