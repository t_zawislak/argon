CXX = g++
CXXFLAGS =  -pedantic -std=c++11

SRCS = main.cpp argon.cpp functions.cpp
OBJS = $(SRCS:.c=.o)


all: argon

argon: $(OBJS)
	$(CXX) $(CXXFLAGS) -o $@ $^

clean:
	rm -f *.o argon

.PHONY: all clean
