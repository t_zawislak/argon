#include "argon.h"


int main(){

  Parameters pars;
  State st( pars.N );

  SetInitialCoordAndMomenta( pars, st);
  CalculateVPF(pars, st);

  FILE* p_output = fopen( ( p_output_path/*+"_t"+std::to_string(t)*/ +".dat").c_str(), "w");
  FILE* r_output0 = fopen( ( r_output_path/*+"_t"+std::to_string(t)*/ +".dat").c_str(), "w");
  fclose( r_output0 );
  FILE* r_output = fopen( ( r_output_path/*+"_t"+std::to_string(t)*/ +".dat").c_str(), "a");
  FILE* r_outputinit = fopen( ( r_output_path_init/*+"_t"+std::to_string(t)*/ +".dat").c_str(), "w");
  FILE* F_output = fopen( ( F_output_path/*+"_t"+std::to_string(t)*/+".dat").c_str(), "w");
  FILE* St_output0 = fopen( ( State_output_path/*+"_t"+std::to_string(t)*/+".dat").c_str(), "w");
  fclose( St_output0 );
  FILE* St_output = fopen( ( State_output_path/*+"_t"+std::to_string(t)*/+".dat").c_str(), "a");


  st.printCoordinates(r_outputinit, 0.5);
  st.printMomenta(p_output, 0.4);
  st.printForces(F_output, 0.4);
  fprintf(St_output, "t\tH\tV\tT\tP\n");


  double Tbar=0, Pbar=0, Hbar=0;

  //===================== MAIN LOOP ======================
  for (int s=1; s <= pars.S_o + pars.S_d; ++s) {
    double t = s*pars.tau;
  //  printf("T = %.3le\t P = %.3le\t H = %.3le\t V = %.3le\n", st.T, st.P, st.H, st.V );
    OneIteration(pars, st);



    if( s >= pars.S_o ){
      Tbar += st.T;
      Pbar += st.P;
      Hbar += st.H;

      if( s % pars.S_out == 0){
        st.printPVHTt(St_output, t);
      }
    }

    if( s % pars.S_xyz == 0){
      st.printCoordinates(r_output, s*0.5);
    }

  //printf("%i\t", s);
  }//

//  printf("Averaged:\n T = %.3le\n P = %.3le\n H = %.3le\n V = %.3le\n", Tbar/pars.S_d, Pbar/pars.S_d, Hbar/pars.S_d, st.V );
  printf("Averaged:\n V = %.3le\n", st.V );







  fclose( p_output );
  fclose( r_output );
  fclose( F_output );
  fclose( St_output );

  return 0;
}
















//
